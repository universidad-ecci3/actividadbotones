package com.example.actbotones

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.EditText
import android.widget.ImageButton
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val boton1= findViewById<ImageButton>(R.id.boton1)
        val boton2= findViewById<ImageButton>(R.id.boton2)
        val boton3= findViewById<ImageButton>(R.id.boton3)
        val boton4= findViewById<ImageButton>(R.id.boton4)
        val boton5= findViewById<ImageButton>(R.id.boton5)
        val msnerror= findViewById<TextView>(R.id.nombre)
        val editnombre= findViewById<EditText>(R.id.editnombre)
        val mensaje: String =" ¡Excelente! Ahora escribe tu nombre y pulsa el "
        val mensaje2: String =" ¡Excelente! Gracias por participar "


        boton1.setOnClickListener {
            val nombreP: String = editnombre.text.toString()



            if (nombreP==""){
                msnerror.text="Escribe tu nombre por favor"
            } else {
                msnerror.setText(editnombre.text.toString()+mensaje+"2")
                editnombre.setText("")
                boton1.setBackgroundColor(resources.getColor(R.color.Check))
                msnerror.setTextColor(resources.getColor(R.color.boton1))
            }

        }

        boton2.setOnClickListener {
            val nombreP: String = editnombre.text.toString()




            if (nombreP==""){
                msnerror.text="Escribe tu nombre por favor"
            } else {
                msnerror.setText(editnombre.text.toString()+mensaje+"3")
                editnombre.setText("")
                boton2.setBackgroundColor(resources.getColor(R.color.Check))
                msnerror.setTextColor(resources.getColor(R.color.boton2))
            }

        }
        boton3.setOnClickListener {
            val nombreP: String = editnombre.text.toString()


            if (nombreP==""){
                msnerror.text="Escribe tu nombre por favor"
            } else {
                msnerror.setText(editnombre.text.toString()+mensaje+"4")
                editnombre.setText("")
                boton3.setBackgroundColor(resources.getColor(R.color.Check))
                msnerror.setTextColor(resources.getColor(R.color.boton3))
            }

        }
        boton4.setOnClickListener {
            val nombreP: String = editnombre.text.toString()

            if (nombreP==""){
                msnerror.text="Escribe tu nombre por favor"
            } else {
                msnerror.setText(editnombre.text.toString()+mensaje+"5")
                editnombre.setText("")
                boton4.setBackgroundColor(resources.getColor(R.color.Check))
                msnerror.setTextColor(resources.getColor(R.color.boton4))

            }

        }
        boton5.setOnClickListener {
            val nombreP: String = editnombre.text.toString()
            boton5.setBackgroundColor(Color.GREEN)
            msnerror.setTextColor(resources.getColor(R.color.boton5))

            if (nombreP==""){
                msnerror.text="Escribe tu nombre por favor"
            } else {
                msnerror.setText(editnombre.text.toString()+mensaje2)
                editnombre.setText("")
                boton5.setBackgroundColor(resources.getColor(R.color.Check))
                msnerror.setTextColor(resources.getColor(R.color.boton5))
            }

        }
    }
}